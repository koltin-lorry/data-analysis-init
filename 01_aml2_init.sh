#!/bin/bash

N4J_HOME="/home/ec2-user/neo4j"
N4J_SCRIPT="$N4J_HOME/run.sh"
N4J_SERVICE="/etc/systemd/system/n4j.service"

JUPYTER_HOME="/home/ec2-user/jupyter"
JUPYTER_SCRIPT="$JUPYTER_HOME/run.sh"
JUPYTER_SERVICE="/etc/systemd/system/jupyter.service"
JUPYTER_TOKEN="secret"

USER="ec2-user"

## install everything we need
sudo yum update
sudo yum install docker -y
sudo su -c "pip3 install jupyter jupyterlab pandas --user" $USER

## create home directories
mkdir $N4J_HOME
mkdir $JUPYTER_HOME
mkdir $JUPYTER_HOME/notebooks

## change permissions
sudo chown $USER -R JUPYTER_HOME
sudo chown $USER -R N4J_HOME

## create startup scripts

echo "#!/bin/bash" >> $N4J_SCRIPT
echo "sudo docker run --name neo4j -p7474:7474 -p7687:7687 -d -v /home/neo4j/data:/data \\" >> $N4J_SCRIPT
echo "-v /home/neo4j/logs:/logs -v /home/neo4j/import:/var/lib/neo4j/import \\" >> $N4J_SCRIPT
echo "-v /home/neo4j/plugins:/plugins --env NEO4J_AUTH=neo4j/1234 neo4j:latest" >> $N4J_SCRIPT

echo "#!/bin/bash" >> $JUPYTER_SCRIPT
echo "export JUPYTER_TOKEN=$JUPYTER_TOKEN" >> $JUPYTER_SCRIPT
echo "/home/$USER/.local/bin/jupyter lab --notebook-dir=$JUPYTER_HOME/notebooks" >> $JUPYTER_SCRIPT

## make them executable
chmod +x $N4J_SCRIPT
chmod +x $JUPYTER_SCRIPT

# create service scripts
echo "[Unit]" >> $N4J_SERVICE
echo "Description=Run neo4j as service" >> $N4J_SERVICE
echo "[Service]" >> $N4J_SERVICE
echo "ExecStart=$N4J_SCRIPT" >> $N4J_SERVICE
echo "[Install]" >> $N4J_SERVICE
echo "WantedBy=multi-user.target" >> $N4J_SERVICE

echo "[Unit]" >> $JUPYTER_SERVICE
echo "Description=Run jupyter lab as service" >> $JUPYTER_SERVICE
echo "[Service]" >> $JUPYTER_SERVICE
echo "User=$USER" >> $JUPYTER_SERVICE
echo "ExecStart=$JUPYTER_SCRIPT" >> $JUPYTER_SERVICE
echo "[Install]" >> $JUPYTER_SERVICE
echo "WantedBy=multi-user.target" >> $JUPYTER_SERVICE

## Register and start services
sudo systemctl enable docker
sudo systemctl start docker

sudo systemctl enable n4j
sudo systemctl start n4j

sudo systemctl enable jupyter
sudo systemctl start jupyter